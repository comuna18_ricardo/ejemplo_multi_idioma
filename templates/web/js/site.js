(function($){

    var sumicorp = {

        /*
            Init Function
        */

        init: function(){
            sumicorp.preloader();
            sumicorp.revolutionSliderHome();
            sumicorp.carrouselDestacados();
            sumicorp.carrouselArticulos();
            sumicorp.fancy();
            sumicorp.initMenu();
            sumicorp.resizeWindow();
            sumicorp.mansory();
            sumicorp.iconos();
        },
        preloader: function() {
            $(window).load(function() {
                $('#status').delay(100).fadeOut('slow');
                $('#preloader').delay(500).fadeOut('slow');
                $('body').delay(500).css({'overflow':'visible'});
                setTimeout(function(){$('.navbar-brand').addClass('animated fadeInDown')},500);
                setTimeout(function(){$('.contieneNavs').addClass('animated fadeInDown')},600);
            })
        },
        revolutionSliderHome: function () {
            $('.fullwidthabnner-inicio, .fullwidthabnner-500, .fullwidthabnner-320').revolution({
                delay:5000,
                startheight:500,
                startwidth:1170,

                hideThumbs:10,

                thumbWidth:100,                         // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                thumbHeight:50,
                thumbAmount:5,

                navigationType:"both",                  //bullet, thumb, none, both     (No Thumbs In FullWidth Version !)
                navigationArrows:"verticalcentered",        //nexttobullets, verticalcentered, none
                navigationStyle:"round",                //round,square,navbar

                touchenabled:"on",                      // Enable Swipe Function : on/off
                onHoverStop:"on",                       // Stop Banner Timet at Hover on Slide on/off

                navOffsetHorizontal:0,
                navOffsetVertical:20,

                stopAtSlide:-1,
                stopAfterLoops:-1,

                shadow:0,                               //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                fullWidth:"on"                          // Turns On or Off the Fullwidth Image Centering in FullWidth Modus
            });
        },
        carrouselDestacados: function () {
            $("#owl-demo-2").owlCarousel({
                items : 4,
                lazyLoad : true,
                pagination: false,
                navigation : true,
                navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            }); 
        },
        carrouselArticulos: function () {
            $("#owl-demo").owlCarousel({
                items : 3,
                lazyLoad : true,
                pagination: false,
                navigation : true,
                navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            }); 
        },
        fancy: function () {
            $('.fancybox-media')
                .attr('rel', 'media-gallery')
                .fancybox({
                    openEffect : 'none',
                    closeEffect : 'none',
                    prevEffect : 'none',
                    nextEffect : 'none',

                    arrows : false,
                    helpers : {
                        media : {},
                        buttons : {}
                    }
                });
            //Galeria Provicional
            $('.gal-empresa').fancybox({
                
            });
            //Producto mapa
            $(".various").fancybox({
                    maxWidth    : 800,
                    maxHeight   : 600,
                    fitToView   : false,
                    //width       : '70%',
                    //height      : '70%',
                    autoSize    : true,
                    closeClick  : false,
                    openEffect  : 'none',
                    closeEffect : 'none'
                });
        },
        menuStruct: undefined,
        initMenu: function() {
            if(this.menuStruct==undefined)
                    this.menuStruct = $("#sitio").html();

            if($( window ).width() <= 757){
                $("#botonMovil").show();
                $("#sitio").mmenu();
            } else {
                $("#botonMovil").hide();
                $("#sitio").remove(); $("#navegacion-superior").append( '<div id="sitio" class="col-lg-8 col-md-8 col-sm-8 ">' + this.menuStruct + '</div>' );
            }
        },
        resizeWindow: function () {
            var initMenu = this.initMenu;
            $( window ).resize(function(){
                initMenu();
            });
        },
        mansory: function () {
            var $container = $('#container-empresa');
            // initialize
            $container.masonry({
              columnWidth: 5,
              itemSelector: '.item',
              isFitWidth: true
            });
        },
        iconos: function(){
            if ($("body").hasClass('inicio')) {
                $('img.imageicono').on('hover', function() {
                     var src = $(this).attr('src');
                     if (src.indexOf(".jpg") > -1) {
                         src = src.replace('.jpg','.gif' );
                     } else {
                         src = src.replace('.gif','.jpg' );
                     }
                     $(this).attr('src', src);
                });
            };
        }
    }
    $(function() {
        sumicorp.init();
    });
})(jQuery);

