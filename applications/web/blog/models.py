from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

from redactor.fields import RedactorField
from filer.fields.image import FilerImageField

from audit_log.models.fields import CreatingUserField

from django.utils.translation import ugettext_lazy as _

from .redactor_config import REDACTOR_MIN, REDACTOR_ADV
from django.conf import settings

class Category(models.Model):
    class Meta:
        verbose_name = _('Categoria')
        verbose_name_plural = _('Categorias')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    name = models.CharField(
        u'Nombre',
        blank=False,
        max_length=256,
        default='',
        help_text='Nombre de la Categoria'
    )
    slug = models.SlugField(
        u'Slug',
        blank=False,
        max_length=256,
        default='',
        help_text='Texto para URL de la Categoria'
    )
    language = models.CharField(max_length=16, choices=getattr(settings, "LANGUAGES", None), default=getattr(settings, "LANGUAGE_CODE", None))
    description = RedactorField(
        verbose_name=u'Descripcion de la categoria',
        allow_file_upload=False,
        allow_image_upload=False,
    )
    publish_date = models.DateTimeField(
        u'Fecha de Creacion',
        blank=True,
        default=timezone.now,
        help_text=u'Fecha de Publicacion'
    )

    def get_absolute_url(self):
        return reverse(
            'blog:category_detail',
            kwargs={'slug': self.slug, }
        )


class Article(models.Model):
    class Meta:
        verbose_name = _('Articulo')
        verbose_name_plural = _('Articulos')

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    title = models.CharField(
        u'Titulo',
        blank=False,
        max_length=256,
        default='',
        help_text='Titulo del Articulo'
    )
    slug = models.SlugField(
        u'Slug',
        blank=False,
        max_length=256,
        default='',
        help_text='Texto para URL del Articulo'
    )
    language = models.CharField(max_length=16, choices=getattr(settings, "LANGUAGES", None), default=getattr(settings, "LANGUAGE_CODE", None))
    intro = RedactorField(
        verbose_name=u'Introduccion',
        allow_file_upload=False,
        default='',
        redactor_options=REDACTOR_MIN
    )
    content = RedactorField(
        verbose_name=u'Contenido',
        allow_file_upload=True,
        default='',
        redactor_options=REDACTOR_ADV
    )
    image_widget = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='article_image_widget'
    )
    image_list = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='article_image_list'
    )
    image_article = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='article_image_article'
    )
    categories = models.ManyToManyField(Category)
    meta_title = models.CharField(
        u'Meta Title',
        blank=True,
        max_length=256,
        default='',
        help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)'
    )
    meta_description = models.TextField(
        u'Meta Description',
        blank=True,
        default='',
        help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)'
    )
    dcm_subject = models.CharField(
        u'DCM: Subject',
        blank=True,
        default='',
        max_length=512,
        help_text='Dublin Core Metadata: Subject'
    )
    dcm_keywords = models.CharField(
        u'DCM: Keywords',
        blank=True,
        default='',
        max_length=512,
        help_text='Dublin Core Metadata: Keywords'
    )
    dcm_source = models.CharField(
        u'DCM: Source',
        blank=True,
        default='',
        max_length=512,
        help_text='Dublin Core Metadata: Source'
    )
    publish_date = models.DateTimeField(
        u'Fecha de Publicacion',
        blank=True,
        default=timezone.now,
        help_text=u'Fecha de Publicacion'
    )
    created_by = CreatingUserField(related_name='created_article')

    def get_absolute_url(self):
        return reverse(
            'blog:articles_detail',
            kwargs={'slug': self.slug, }
        )

