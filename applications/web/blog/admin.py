from django.contrib import admin

from .models import Article, Category


class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields =  {'slug': ('title',)}
    filter_horizontal = ('categories',)

    fieldsets = (
        (None, {
            'fields': (
            	'title',
            	'slug', 
            	'intro', 
            	'content', 
                'language', 
            )
        }),
        ('Imagenes', {
            'fields': ('image_widget', 'image_list', 'image_article',)
        }),
        ('Categorias', {
            'classes': ('collapse',),
            'fields': ('categories',)
        }),
        ('Metadata', {
            'classes': ('collapse',),
            'fields': (
            	'meta_title', 
            	'meta_description', 
            	'dcm_subject', 
            	'dcm_keywords',
            	'dcm_source',
            	'publish_date',
            )
        })
	)


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields =  {'slug': ('name',)}


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)

