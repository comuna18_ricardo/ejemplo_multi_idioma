#from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView

from .models import Article, Category


class CategoryDetailView(DetailView):
    model = Category
    template_name = 'blog/article_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        articles = Article.objects.filter(categories__id=context['object'].pk)

        context.update({'object_list':articles})

        return context


class ArticlesListView(ListView):
    model = Article


class ArticleDetailView(DetailView):
    model = Article

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        curr_article_pk = context['object'].pk

        try:
            next_article = Article.objects.filter(id__gt=curr_article_pk)[0]
        except IndexError:
            next_article = False

        try:
            prev_article = Article.objects.filter(id__lt=curr_article_pk)[0]
        except IndexError:
            prev_article = False

        context.update({'prev_article': prev_article, 'next_article': next_article})

        return context


