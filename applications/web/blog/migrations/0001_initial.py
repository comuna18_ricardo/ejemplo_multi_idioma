# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import redactor.fields
from django.conf import settings
import filer.fields.image
import audit_log.models.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=256, help_text='Titulo del Articulo', verbose_name='Titulo', default='')),
                ('slug', models.SlugField(max_length=256, help_text='Texto para URL del Articulo', verbose_name='Slug', default='')),
                ('intro', redactor.fields.RedactorField(verbose_name='Introduccion', default='')),
                ('content', redactor.fields.RedactorField(verbose_name='Contenido', default='')),
                ('meta_title', models.CharField(blank=True, max_length=256, help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)', verbose_name='Meta Title', default='')),
                ('meta_description', models.TextField(blank=True, help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)', verbose_name='Meta Description', default='')),
                ('dcm_subject', models.CharField(blank=True, max_length=512, help_text='Dublin Core Metadata: Subject', verbose_name='DCM: Subject', default='')),
                ('dcm_keywords', models.CharField(blank=True, max_length=512, help_text='Dublin Core Metadata: Keywords', verbose_name='DCM: Keywords', default='')),
                ('dcm_source', models.CharField(blank=True, max_length=512, help_text='Dublin Core Metadata: Source', verbose_name='DCM: Source', default='')),
                ('publish_date', models.DateTimeField(blank=True, help_text='Fecha de Publicacion', verbose_name='Fecha de Publicacion', default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'Articulo',
                'verbose_name_plural': 'Articulos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=256, help_text='Nombre de la Categoria', verbose_name='Nombre', default='')),
                ('slug', models.SlugField(max_length=256, help_text='Texto para URL de la Categoria', verbose_name='Slug', default='')),
                ('description', redactor.fields.RedactorField(verbose_name='Descripcion de la categoria')),
                ('publish_date', models.DateTimeField(blank=True, help_text='Fecha de Publicacion', verbose_name='Fecha de Creacion', default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='article',
            name='categories',
            field=models.ManyToManyField(to='blog.Category'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, related_name='created_article', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='image_article',
            field=filer.fields.image.FilerImageField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article_image_article', to='filer.Image'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='image_list',
            field=filer.fields.image.FilerImageField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article_image_list', to='filer.Image'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='image_widget',
            field=filer.fields.image.FilerImageField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article_image_widget', to='filer.Image'),
            preserve_default=True,
        ),
    ]
