# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='language',
            field=models.CharField(choices=[('es', 'es'), ('en', 'en')], max_length=16, default='es'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='language',
            field=models.CharField(choices=[('es', 'es'), ('en', 'en')], max_length=16, default='es'),
            preserve_default=True,
        ),
    ]
