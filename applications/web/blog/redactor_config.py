REDACTOR_MIN = {
    'formatting': ['p', 'h1', 'h2', 'h3'],
    'formattingAdd': [
        {
            'tag': 'strong',
            'title': 'strong'
        }
    ],
    'buttons': ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist'],
    'buttonSource': 'true'
}

REDACTOR_ADV = {
    #'buttons': ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist'],
    'buttonSource': 'true',
    'plugins': ['video']
}