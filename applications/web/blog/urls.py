from django.conf.urls import patterns, url

from .views import ArticlesListView, ArticleDetailView, CategoryDetailView#, CategoriesListView


urlpatterns = patterns('',
    url(r'^categoria/(?P<slug>.*)/$', CategoryDetailView.as_view(), name='category_detail'),
    url(r'^$', ArticlesListView.as_view(), name="articles_list"),
    url(r'^(?P<slug>.*)/$', ArticleDetailView.as_view(), name='articles_detail'),
    #url(r'^$', CategoriesListView.as_view(), name="categories_list"),
    #url(r'^(?P<slug>.*)/$', CategoryDetailView.as_view(), name='category_detail'),
)

