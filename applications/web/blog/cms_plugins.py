from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin

from .models import Article


class ArticlesCMSPlugin(CMSPluginBase):
    model = CMSPlugin
    name = u'Articulos Plugin'
    render_template = 'plugins/articles.html'

    def render(self, context, instance, placeholder):
        context['articles'] = Article.objects.all()[:3]
        return context

plugin_pool.register_plugin(ArticlesCMSPlugin)

