# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cms.models.pluginmodel


class Migration(migrations.Migration):

    dependencies = [
        #('cms', '0004_auto_20150326_1510'),
    ]

    operations = [
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(primary_key=True, to='cms.CMSPlugin', serialize=False, parent_link=True, auto_created=True)),
                ('image', models.ImageField(verbose_name='image', upload_to=cms.models.pluginmodel.get_plugin_media_path)),
                ('url', models.CharField(max_length=255, help_text='If present, clicking on image will take user to link.', blank=True, verbose_name='link', null=True)),
                ('alt', models.CharField(max_length=255, help_text='Specifies an alternate text for an image, if the imagecannot be displayed.<br />Is also used by search enginesto classify the image.', blank=True, verbose_name='alternate text', null=True)),
                ('longdesc', models.CharField(max_length=255, help_text='When user hovers above picture, this text will appear in a popup.', blank=True, verbose_name='long description', null=True)),
                ('float', models.CharField(max_length=10, blank=True, verbose_name='side', help_text='Move image left, right or center.', choices=[('left', 'left'), ('right', 'right'), ('center', 'center')], null=True)),
                ('page_link', models.ForeignKey(to='cms.Page', blank=True, verbose_name='page', null=True, help_text='If present, clicking on image will take user to specified page.')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
