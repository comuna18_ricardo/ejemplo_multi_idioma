from __future__ import print_function
from cms.sitemaps import CMSSitemap
from django.conf.urls import *  # NOQA
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, url

#sitemap
from django.contrib.sitemaps import GenericSitemap
from django.contrib.sitemaps.views import sitemap
from cms_extensions.CMSSitemap import CMSSitemap_custom

from blog.models import Article, Category
from products.models import Product
from industries.models import Industry
# from services.models import Service
# from faq.models import Sections, Question

admin.autodiscover()

articles_dict = {
    'queryset': Article.objects.all(),
    'date_field': 'publish_date',
}

categories_dic = {
    'queryset': Category.objects.all(),
    'date_field': 'publish_date',
}
products_dic = {
    'queryset': Product.objects.all(),
    'date_field': 'publish_date',
}
industries_dic = {
    'queryset': Industry.objects.all(),
    'date_field': 'publish_date',
}

#services_dic = {
#    'queryset': Service.objects.all(),
#    'date_field': 'publish_date',   
#}

#sections_dic = {
#    'queryset': Sections.objects.all(),
#    'date_field': 'create_date'
#}

#questions_dic = {
#    'queryset': Question.objects.all(),
#    'date_field': 'create_date'
#}

sitemaps = {
    'flatpages': CMSSitemap_custom,
    'blog': GenericSitemap(articles_dict, priority=0.6),
    'categorias': GenericSitemap(categories_dic, priority=0.6),
    'producto': GenericSitemap(products_dic, priority=0.6),
    'industria': GenericSitemap(industries_dic, priority=0.6),
    #'servicios': GenericSitemap(services_dic, priority=0.6),
    #'preguntas': GenericSitemap(questions_dic, priority=0.6),
    #'secciones': GenericSitemap(sections_dic, priority=0.6),
}

urlpatterns = i18n_patterns('',
    url(r'^uQwBke/', include(admin.site.urls)),  # NOQA
   
    #url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^', include('cms.urls')),
)
urlpatterns += patterns('',
	url(r'^redactor/', include('redactor.urls')),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA
