import os
gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))
"""
Django settings for sumicorpcommx project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '5njb*=utrt0%tr*)t+3jc@ik(ae$vunj^2m*57&c#$w+#od7ba'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition





ROOT_URLCONF = 'sumicorpcommx.urls'

WSGI_APPLICATION = 'sumicorpcommx.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases



# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Mexico_City'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(DATA_DIR, 'media')
STATIC_ROOT = os.path.join(DATA_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'sumicorpcommx', 'static'),
)
SITE_ID = 1
SITE_URL = "www.sumicorp.com.mx"

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'audit_log.middleware.UserLoggingMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.csrf',
    'django.core.context_processors.tz',
    'sekizai.context_processors.sekizai',
    'django.core.context_processors.static',
    'cms.context_processors.cms_settings',
    'cms_extensions.context_processors.site',
    'cms_extensions.context_processors.mobile'
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'sumicorpcommx', 'templates'),
)

INSTALLED_APPS = (
    'djangocms_admin_style',
    'djangocms_text_ckeditor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'menus',
    'sekizai',
    'mptt',
    'djangocms_style',
    'djangocms_column',
    'djangocms_file',
    'djangocms_flash',
    'djangocms_googlemap',
    'djangocms_inherit',
    'djangocms_link',
    #'djangocms_picture',
    'djangocms_teaser',
    'djangocms_video',
    'reversion',
    'easy_thumbnails',
    'filer',
    'redactor',
    'cms_picture',
    'widget_tweaks',
    'parler',
    'tinymce',
    'sumicorpcommx',
    'sliders',
    'contacts',
    'blog',
    'products',
    'industries',
    'gallerys',
    'services',
    'videos',
    'cms_extensions'
)

LANGUAGES = (
    ## Customize this
    ('es', gettext('es')),
    ('en', gettext('en')),
)

CMS_LANGUAGES = {
    ## Customize this
    1: [
        {
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
            'name': gettext('es'),
            'code': 'es',
        },
        {
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
            'name': gettext('en'),
            'code': 'en',
        },
    ],
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_TEMPLATES = (
    ## Customize this
    ('fullwidth.html', 'Fullwidth'),
    ('layouts/inicio.html', u'Inicio'),
    ('layouts/empresa.html', u'Empresa'),
    ('layouts/articulos.html', u'Articulos'),
    ('layouts/servicios.html', u'Servicios'),
    ('layouts/industrias.html', u'Industrias'),
    ('layouts/que-necesitas.html', u'Que Necesitas'),
    ('layouts/productos-main.html', u'Productos (Principal)'),
    ('layouts/productos-list.html', u'Productos (Listado)'),
    ('layouts/catalogo.html', u'Catalogo'),
    ('layouts/contacto.html', u'Contacto'),
    ('layouts/contacto-submit.html', u'Contacto (Enviado)'),
)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {}

DATABASES = {
    'default':
        {'PASSWORD': 'cG7M46NKwxAZ', 'NAME': 'wwwsumicorpcommx', 'USER': 'sumicorp', 'HOST': 'dalek-001.cvn3iw1qgpcc.us-west-2.rds.amazonaws.com', 'PORT': 5432, 'ENGINE': 'django.db.backends.postgresql_psycopg2'}
}

MIGRATION_MODULES = {
    'cms': 'cms.migrations_django',
    'menus': 'menus.migrations_django',
    'djangocms_text_ckeditor': 'djangocms_text_ckeditor.migrations_django',
    'djangocms_column': 'djangocms_column.migrations_django',
    'djangocms_flash': 'djangocms_flash.migrations_django',
    'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
    'djangocms_inherit': 'djangocms_inherit.migrations_django',
    'djangocms_style': 'djangocms_style.migrations_django',
    'djangocms_file': 'djangocms_file.migrations_django',
    'djangocms_link': 'djangocms_link.migrations_django',
    'djangocms_picture': 'djangocms_picture.migrations_django',
    'djangocms_teaser': 'djangocms_teaser.migrations_django',
    'djangocms_video': 'djangocms_video.migrations_django',
    'filer': 'filer.migrations_django'
}

# Aqui se encuentran las configuraciones de Redactor
REDACTOR_OPTIONS = {'lang': 'es'}
REDACTOR_UPLOAD = 'uploads/'

REDACTOR_MIN = {
    'formatting': ['p', 'h1', 'h2', 'h3'],
    'formattingAdd': [
        {
            'tag': 'strong',
            'title': 'strong'
        }
    ],
    'buttons': ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist'],
    'buttonSource': 'true'
}

REDACTOR_ADV = {
    #'buttons': ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist'],
    'buttonSource': 'true',
    #'plugins': ['video']
}

#settings for database backup
HOST_INFO = {
    'HOST': 'retardo_production',
    'USER': 'evolutiva'
}

PARLER_DEFAULT_LANGUAGE = 'es'

PARLER_LANGUAGES = {
    1: (
        {'code': 'es'},
        {'code': 'en'},
    ),
    'default': {}
}
