$(document).ready(function(){
  var label_name = {'es':'Nombre Completo', 'en': 'Full Name'}
    , label_company = {'es':'Compañia', 'en': 'Company'}
    , label_telephone = {'es':'Telefono', 'en': 'Telephone'}
    , label_email = {'es':'Email', 'en': 'Email'}
    , label_message = {'es':'Mensaje', 'en': 'Message'}
    , label_country = {'es':'Pais', 'en': 'Country'}
    , lang_widg = $('#form-contact-modal-widget-lang').val()
    , label_number_code = {'es':'Código País + Código Ciudad + Número Telefónico', 'en':'Country code + City code + Phone number'}

    $('#contact-plugin-modal-form #id_name').attr('placeholder', label_name[lang_widg])
    $('#contact-plugin-modal-form #id_name').siblings('label').html(label_name[lang_widg] + ':')

    $('#contact-plugin-modal-form #id_company').attr('placeholder', label_company[lang_widg])
    $('#contact-plugin-modal-form #id_company').siblings('label').html(label_company[lang_widg] + ':')

    $('#contact-plugin-modal-form #id_telephone').attr('placeholder', label_telephone[lang_widg])
    $('#contact-plugin-modal-form #id_telephone').siblings('label').html(label_telephone[lang_widg] + ':')

    $('#contact-plugin-modal-form #id_email').attr('placeholder', label_email[lang_widg])
    $('#contact-plugin-modal-form #id_email').siblings('label').html(label_email[lang_widg] + ':')

    $('#contact-plugin-modal-form #id_comments').attr('placeholder', label_message[lang_widg])
    $('#contact-plugin-modal-form #id_comments').siblings('label').html(label_message[lang_widg] + ':')

    $('#contact-plugin-modal-form #id_country').siblings('label').html(label_country[lang_widg] + ':')
    
    $('#contact-plugin-modal-form textarea').attr('rows', '2');

    $('#contact-plugin-modal-form #id_acepto_aviso_de_privacidad').remove();

    //link de donde se manda el correo
    $('#contact-plugin-modal-form #id_send_to').val(document.URL);

    //validacion de seleccion de pais
    $('#contact-plugin-modal-form #id_telephone').prop("disabled", true);
    $('#contact-plugin-modal-form #id_country').change(function(){
      var country_val = $('#contact-plugin-modal-form #id_country').val();
      if (country_val == '' || country_val == undefined) {
        $('#contact-plugin-modal-form #id_telephone').prop("disabled", true);
      } else {
        $('#contact-plugin-modal-form #id_telephone').prop("disabled", false);
      }
    })

    //masked input
    //$("#contact-plugin-modal-form #id_telephone").mask("(999) 999-9999", {placeholder:"(999) 999-9999"});
    $('#contact-plugin-modal-form #id_country').change(function(){
      var elval = $('#contact-plugin-modal-form #id_country').val();
      var clear_telephone = $("#contact-plugin-modal-form #id_telephone").val("");
      var html_number_code = " <span id='number_code' style='font-size: 10px;'>" + label_number_code[lang_widg] + "</span> ";
      var delete_number_code = $("#contact-plugin-modal-form #number_code").remove();
      if(elval=='MX'){$("#contact-plugin-modal-form #id_telephone").mask("(***) ***-****", {placeholder:"(***) ***-****"}); delete_number_code; clear_telephone; } else if(elval=='EX'){$("#contact-plugin-modal-form #id_telephone").mask("*** + **** + ********", {placeholder:"*** + **** + ********"}); clear_telephone; $("#contact-plugin-modal-form #id_telephone").html(html_number_code).parent().append(html_number_code); } else{delete_number_code; clear_telephone; }
    })

    $('#contact-plugin-modal-form').submit(function(e){
        return false;
    });
})

(function($){
  "use strict";

  $(function(){

    $('#submit-contact-plugin-modal-form').on('click', function(evt){
      var $form = $('#contact-plugin-modal-form');
      $('#contact-plugin-modal-form span.error').html('');
      function handleResponse(data){

        if (data.pk) { // Success!
          $form.siblings('.success').html(data.success).show(100);
          $('#submit-contact-plugin-modal-form').remove();

          //
          // NOTE: We hide the form if there was ANY success to prevent
          // duplicate submissions. There can be no success if there are
          // form-validation errors, in which case, the form remains
          // visible so the visitor can correct their mistakes.  In the
          // event that there are no validation errors and yet, nothing is
          // successful, the form remains visible so the user can try again.
          //
          $form.add('.legend').hide(100);
        }

        else { // Validation Issues...
          console.log('validation issues')
          
          $.each(data, function(key, value){
            $('#contact-plugin-modal-form input[name="' + key + '"]').siblings('.error').html(value[0])
            $('#contact-plugin-modal-form textarea[name="' + key + '"]').siblings('.error').html(value[0])
          });

        }
      }

      evt.preventDefault();
      $form.siblings('.errors, .success').hide(100);

      $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: $form.serialize()
      }).always(handleResponse);
    });

  });
}(window.jQuery));
