$(document).ready(function(){
	var label_name = {'es':'Nombre Completo', 'en': 'Full Name'}
		, label_company = {'es':'Compañia', 'en': 'Company'}
		, label_telephone = {'es':'Telefono', 'en': 'Telephone'}
    , label_email = {'es':'Email', 'en': 'Email'}
    , label_message = {'es':'Mensaje', 'en': 'Message'}
    , lang_widg = $('#form-contact-widget-lang').val();

	$('#contact-plugin-form #id_name').attr('placeholder', label_name[lang_widg])
	$('#contact-plugin-form #id_company').attr('placeholder', label_company[lang_widg])
	$('#contact-plugin-form #id_telephone').attr('placeholder', label_telephone[lang_widg])
  $('#contact-plugin-form #id_email').attr('placeholder', label_email[lang_widg])
  $('#contact-plugin-form #id_comments').attr('placeholder', label_message[lang_widg])
  $('#contact-plugin-form textarea').attr('rows', '2');


	$('#contact-plugin-form').submit(function(e){
		    return false;
	});
})

(function($){
  "use strict";

  $(function(){

    $('#submit-contact-plugin-form').on('click', function(evt){
      var $form = $(this).parents('form').eq(0);
      $('#contact-plugin-form span.error').html('');
      function handleResponse(data){

        if (data.pk) { // Success!
          $form.siblings('.success').html(data.success).show(100);

          //
          // NOTE: We hide the form if there was ANY success to prevent
          // duplicate submissions. There can be no success if there are
          // form-validation errors, in which case, the form remains
          // visible so the visitor can correct their mistakes.  In the
          // event that there are no validation errors and yet, nothing is
          // successful, the form remains visible so the user can try again.
          //
          $form.add('.legend').hide(100);
        }

        else { // Validation Issues...
          console.log('validation issues')
          
          $.each(data, function(key, value){
            $('#contact-plugin-form input[name="' + key + '"]').siblings('.error').html(value[0])
            $('#contact-plugin-form textarea[name="' + key + '"]').siblings('.error').html(value[0])
          });

        }
      }

      evt.preventDefault();
      $form.siblings('.errors, .success').hide(100);

      $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: $form.serialize()
      }).always(handleResponse);
    });

  });
}(window.jQuery));
