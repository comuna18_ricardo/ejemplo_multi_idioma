# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sliders', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='content',
            field=redactor.fields.RedactorField(verbose_name='Contenido', default=''),
            preserve_default=True,
        ),
    ]
