# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        #('cms', '0004_auto_20150326_1510'),
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('content_position', models.CharField(max_length=16, choices=[('center', 'Content to center'), ('left', 'Content to left'), ('without', 'Without content')])),
                ('image', filer.fields.image.FilerImageField(related_name='slide_image', to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SliderPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, to='cms.CMSPlugin', serialize=False, primary_key=True, parent_link=True)),
                ('slider', models.ForeignKey(to='sliders.Slider')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.AddField(
            model_name='slide',
            name='slider',
            field=models.ForeignKey(to='sliders.Slider'),
            preserve_default=True,
        ),
    ]
