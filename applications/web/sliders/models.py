from django.db import models
from django.conf import settings

from cms.models import CMSPlugin

from redactor.fields import RedactorField
from filer.fields.image import FilerImageField


class Slider(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Slide(models.Model):
    CONTENT_POSITIONS = (
        ('center', 'Content to center'),
        ('left', 'Content to left'),
        ('without', 'Without content'),
    )

    slider = models.ForeignKey(Slider)
    image = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='slide_image'
    )
    content = RedactorField( 
        verbose_name=u'Contenido',
        allow_file_upload=False,
        allow_image_upload=False,
        default='',
        redactor_options=settings.REDACTOR_MIN
    )
    content_position = models.CharField(max_length=16, choices=CONTENT_POSITIONS)


class SliderPlugin(CMSPlugin):
    slider = models.ForeignKey(Slider)
