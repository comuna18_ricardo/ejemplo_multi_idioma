from django.contrib import admin

from .models import Slider, Slide


class SlideInline(admin.StackedInline):
    model = Slide
    extra = 1


class SliderAdmin(admin.ModelAdmin):
    inlines = [SlideInline, ]


class SlideAdmin(admin.ModelAdmin):
    pass


admin.site.register(Slider, SliderAdmin)
admin.site.register(Slide, SlideAdmin)
