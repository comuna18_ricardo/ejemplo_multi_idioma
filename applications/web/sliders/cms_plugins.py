from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import SliderPlugin


class CMSSliderPlugin(CMSPluginBase):
    model = SliderPlugin
    name = u'Slider'
    render_template = 'slider.html'

    def render(self, context, instance, placeholder):
        context.update({
            'slider': instance.slider,
            'object': instance,
            'placeholder': placeholder
        })

        return context


plugin_pool.register_plugin(CMSSliderPlugin)
