from django.contrib import admin

from .models import Gallery, Image


class ImageInline(admin.StackedInline):
    model = Image
    extra = 1


class GalleryAdmin(admin.ModelAdmin):
    inlines = [ImageInline]


class ImageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Image, ImageAdmin)
admin.site.register(Gallery, GalleryAdmin)
