# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        #('cms', '0004_auto_20150326_1510'),
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GalleryPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, serialize=False, auto_created=True, to='cms.CMSPlugin', primary_key=True)),
                ('gallery', models.ForeignKey(to='gallerys.Gallery')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('width', models.CharField(max_length=16, choices=[('w1', 'w1'), ('w2', 'w2')])),
                ('gallery', models.ForeignKey(to='gallerys.Gallery')),
                ('image', filer.fields.image.FilerImageField(on_delete=django.db.models.deletion.SET_NULL, null=True, to='filer.Image')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
