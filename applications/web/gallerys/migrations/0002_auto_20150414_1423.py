# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('gallerys', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='image',
            field=filer.fields.image.FilerImageField(to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, related_name='gallery_image', null=True),
            preserve_default=True,
        ),
    ]
