from django.db import models

from cms.models import CMSPlugin
from filer.fields.image import FilerImageField


class Gallery(models.Model):
    name = models.CharField(max_length=128)
    
    def __str__(self):
        return self.name
    

class Image(models.Model):
    WIDTH_OPTIONS = (('w1', 'w1'), ('w2', 'w2'),)

    gallery = models.ForeignKey(Gallery)
    image = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='gallery_image'
    )
    width = models.CharField(max_length=16, choices=WIDTH_OPTIONS)

    def __str__(self):
        return self.id
    
    
class GalleryPlugin(CMSPlugin):
    gallery = models.ForeignKey(Gallery)
