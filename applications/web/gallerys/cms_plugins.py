from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import GalleryPlugin


class CMSGalleryPlugin(CMSPluginBase):
    model = GalleryPlugin
    name = u'Gallery'
    render_template = 'gallery.html'

    def render(self, context, instance, placeholder):
        context.update({
            'gallery': instance.gallery,
            'object': instance,
            'placeholder': placeholder
        })

        return context


plugin_pool.register_plugin(CMSGalleryPlugin)
