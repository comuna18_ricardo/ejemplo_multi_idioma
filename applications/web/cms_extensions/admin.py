from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from cms.extensions import TitleExtensionAdmin, PageExtensionAdmin

#Title Extensions Models
from .models import MetaCanonicalExtension, SEOMetaDataExtension

#Page Extension Models
from .models import PageAttributesExtension, Profile


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profiles'

class UserAdmin(UserAdmin):
    inlines = (ProfileInline, )

class SEOMetaDataExtensionAdmin(TitleExtensionAdmin):
	fieldsets = (
        (None, {
            'fields': ('title', 'description', 'url')
        }),
        ('Open Graph', {
            'classes': ('collapse',),
            'fields': ('og_enabled', 'og_type', 'fb_admins', 'og_imageurl')
        }),
        ('Twitter Cards', {
            'classes': ('collapse',),
            'fields': ('twc_enabled', 'twc_card', 'twc_site', 'twc_creator', 'twc_imageurl')
        }),
        ('Dublin Core Metadata', {
            'classes': ('collapse',),
            'fields': ('dcm_enabled', 'dcm_keywords', 'dcm_subject', 'dcm_language', 'dcm_source')
        })
	)


class MetaCanonicalExtensionAdmin(TitleExtensionAdmin):
	pass

class PageAttributesExtensionAdmin(PageExtensionAdmin):
	pass


#Title Extensions Admin
admin.site.register(SEOMetaDataExtension, SEOMetaDataExtensionAdmin)
admin.site.register(MetaCanonicalExtension, MetaCanonicalExtensionAdmin)

#Page Extension Admin
admin.site.register(PageAttributesExtension, PageAttributesExtensionAdmin)

#User Profile Admin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
