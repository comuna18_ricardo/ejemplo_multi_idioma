from cms.sitemaps import CMSSitemap
from django.db.models import Q
from django.utils import translation
from cms.models import Title


class CMSSitemap_custom(CMSSitemap):

	def items(self):
	    all_titles = Title.objects.public().filter(
	        Q(redirect='') | Q(redirect__isnull=True),
	        page__login_required=False, language='es'
	    ).order_by('page__tree_id', 'page__lft')
	    return all_titles
