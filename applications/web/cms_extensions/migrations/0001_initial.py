# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import filer.fields.image
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        #('cms', '0004_auto_20150326_1510'),
        ('filer', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MetaCanonicalExtension',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('enabled', models.BooleanField(verbose_name='Enable Canonical URL', help_text='Do you want enable Canonical URL?', default=False)),
                ('url', models.CharField(verbose_name='Canonical URL', help_text='Canonical URL', blank=True, default='', max_length=512)),
                ('extended_object', models.OneToOneField(editable=False, to='cms.Title')),
                ('public_extension', models.OneToOneField(editable=False, related_name='draft_extension', null=True, to='cms_extensions.MetaCanonicalExtension')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PageAttributesExtension',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('body_class', models.CharField(verbose_name='Body Class', help_text='CSS classes for body tag', default='', max_length=256)),
                ('extended_object', models.OneToOneField(editable=False, to='cms.Page')),
                ('public_extension', models.OneToOneField(editable=False, related_name='draft_extension', null=True, to='cms_extensions.PageAttributesExtension')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('twitter', models.CharField(verbose_name='Twitter User Name', help_text='usuario', blank=True, default='', max_length=256)),
                ('photo', filer.fields.image.FilerImageField(on_delete=django.db.models.deletion.SET_NULL, null=True, blank=True, to='filer.Image')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SEOMetaDataExtension',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('title', models.CharField(verbose_name='Title', help_text='Titulo para la Pagina', default='', max_length=256)),
                ('description', models.TextField(verbose_name='Description', help_text='Description para la Pagina')),
                ('url', models.CharField(verbose_name='URL', help_text='URL de la pagina, si se deja en blanco se genera automaticamente', blank=True, default='', max_length=256)),
                ('og_enabled', models.BooleanField(verbose_name='Enable Open Graph', help_text='Deseas habilitar Open Graph?', default=False)),
                ('og_type', models.CharField(verbose_name='og:type', help_text='Open Graph: Type', blank=True, default='', max_length=256)),
                ('og_imageurl', models.CharField(verbose_name='og:image url', help_text='Open Graph: URL de la Imagen', blank=True, default='', max_length=512)),
                ('fb_admins', models.CharField(verbose_name='Facebook Admins', help_text='Facebook Admin Code', blank=True, default='', max_length=64)),
                ('twc_enabled', models.BooleanField(verbose_name='Enable Twitter Cards', help_text='Deseas habilitar Twitter Cards?', default=False)),
                ('twc_card', models.CharField(verbose_name='twc:card', help_text='Twitter: Card', blank=True, default='', max_length=256)),
                ('twc_site', models.CharField(verbose_name='twc:site', help_text='Twitter: Cuenta del sitio', blank=True, default='', max_length=256)),
                ('twc_creator', models.CharField(verbose_name='twc:creator', help_text='Twitter: Cuenta del creador', blank=True, default='', max_length=256)),
                ('twc_imageurl', models.CharField(verbose_name='twc:image url', help_text='Twitter Card: URL de la Imagen', blank=True, default='', max_length=512)),
                ('dcm_enabled', models.BooleanField(verbose_name='Enable Dublin Core Metadata (DCM) ', help_text='Deseas habilitar Dublin Core Metadata?', default=False)),
                ('dcm_keywords', models.CharField(verbose_name='Keywords', help_text='DCM: Keywords separed by comma', blank=True, default='', max_length=256)),
                ('dcm_subject', models.CharField(verbose_name='Subject', help_text='DCM: Subject', blank=True, default='', max_length=256)),
                ('dcm_language', models.CharField(verbose_name='Language', help_text='DCM: Language', blank=True, default='', max_length=256)),
                ('dcm_source', models.CharField(verbose_name='Source', help_text='DCM: Source', blank=True, default='', max_length=256)),
                ('extended_object', models.OneToOneField(editable=False, to='cms.Title')),
                ('public_extension', models.OneToOneField(editable=False, related_name='draft_extension', null=True, to='cms_extensions.SEOMetaDataExtension')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
