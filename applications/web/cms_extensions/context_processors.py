import re
from django.conf import settings


def site(request):
    SITE_PROTOCOL_RELATIVE_URL = '//' + settings.SITE_URL
    SITE_PROTOCOL = 'http'
    if request.is_secure():
        SITE_PROTOCOL = 'https'

    SITE_PROTOCOL_URL = SITE_PROTOCOL + '://' + settings.SITE_URL

    return {
        'SITE_URL': settings.SITE_URL,
        'SITE_PROTOCOL': SITE_PROTOCOL,
        'SITE_PROTOCOL_URL': SITE_PROTOCOL_URL,
        'SITE_PROTOCOL_RELATIVE_URL': SITE_PROTOCOL_RELATIVE_URL
    }

def mobile(request):
    is_mobile = False
    if 'HTTP_USER_AGENT' in request.META:
        user_agent = request.META['HTTP_USER_AGENT']
        pattern = "(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|windows ce|pda|mobile|mini|palm|netfront|droid)"
        prog = re.compile(pattern, re.IGNORECASE)
        match = prog.search(user_agent)
        if match:
            is_mobile = True;

    return {
        'is_mobile':is_mobile
    }
