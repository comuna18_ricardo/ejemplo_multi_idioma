from django.db import models

from cms.extensions import TitleExtension, PageExtension
from cms.extensions.extension_pool import extension_pool

from filer.fields.image import FilerImageField

from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User)
    photo = FilerImageField(
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    twitter = models.CharField(
        u'Twitter User Name',
        blank=True,
        max_length=256,
        default='',
        help_text='usuario'
    )


class PageAttributesExtension(PageExtension):
    body_class = models.CharField(
        u'Body Class',
        blank=False,
        default='',
        help_text=u'CSS classes for body tag',
        max_length=256
    )


class MetaCanonicalExtension(TitleExtension):
    enabled = models.BooleanField(u'Enable Canonical URL', blank=False, default=False, help_text='Do you want enable Canonical URL?')
    url = models.CharField(
        u'Canonical URL',
        blank=True,
        default='',
        help_text=u'Canonical URL',
        max_length=512
    )


class SEOMetaDataExtension(TitleExtension):
    title = models.CharField(
        u'Title',
        blank=False,
        default='',
        help_text=u'Titulo para la Pagina',
        max_length=256
    )
    description = models.TextField(
        u'Description',
        blank=False,
        help_text='Description para la Pagina'
    )
    url = models.CharField(
        u'URL',
        blank=True,
        default='',
        help_text=u'URL de la pagina, si se deja en blanco se genera automaticamente',
        max_length=256
    )
    og_enabled = models.BooleanField(u'Enable Open Graph', blank=True, default=False, help_text='Deseas habilitar Open Graph?')
    og_type = models.CharField(
        u'og:type',
        blank=True,
        default='',
        help_text=u'Open Graph: Type',
        max_length=256
    )
    og_imageurl = models.CharField(
        u'og:image url',
        blank=True,
        default='',
        help_text=u'Open Graph: URL de la Imagen',
        max_length=512
    )
    fb_admins = models.CharField(
        u'Facebook Admins',
        blank=True,
        default='',
        help_text=u'Facebook Admin Code',
        max_length=64
    )
    twc_enabled = models.BooleanField(u'Enable Twitter Cards', blank=True, default=False, help_text='Deseas habilitar Twitter Cards?')
    twc_card = models.CharField(
        u'twc:card',
        blank=True,
        default='',
        help_text=u'Twitter: Card',
        max_length=256
    )
    twc_site = models.CharField(
        u'twc:site',
        blank=True,
        default='',
        help_text=u'Twitter: Cuenta del sitio',
        max_length=256
    )
    twc_creator = models.CharField(
        u'twc:creator',
        blank=True,
        default='',
        help_text=u'Twitter: Cuenta del creador',
        max_length=256
    )
    twc_imageurl = models.CharField(
        u'twc:image url',
        blank=True,
        default='',
        help_text=u'Twitter Card: URL de la Imagen',
        max_length=512
    )
    dcm_enabled = models.BooleanField(u'Enable Dublin Core Metadata (DCM) ', blank=True, default=False, help_text='Deseas habilitar Dublin Core Metadata?')
    dcm_keywords = models.CharField(
        u'Keywords',
        blank=True,
        default='',
        help_text=u'DCM: Keywords separed by comma',
        max_length=256
    )
    dcm_subject = models.CharField(
        u'Subject',
        blank=True,
        default='',
        help_text=u'DCM: Subject',
        max_length=256
    )
    dcm_language = models.CharField(
        u'Language',
        blank=True,
        default='',
        help_text=u'DCM: Language',
        max_length=256
    )
    dcm_source = models.CharField(
        u'Source',
        blank=True,
        default='',
        help_text=u'DCM: Source',
        max_length=256
    )

extension_pool.register(MetaCanonicalExtension)
extension_pool.register(SEOMetaDataExtension)
#Page Extensions
extension_pool.register(PageAttributesExtension)

