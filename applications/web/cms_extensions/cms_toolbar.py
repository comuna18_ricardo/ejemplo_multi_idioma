from cms.toolbar_pool import toolbar_pool
from cms.extensions.toolbar import ExtensionToolbar

from django.core.urlresolvers import NoReverseMatch
from cms.utils.urlutils import admin_reverse

from django.utils.translation import ugettext_lazy as _

#Title Extensions Models
from .models import MetaCanonicalExtension, SEOMetaDataExtension

#Page Extensions Models
from .models import PageAttributesExtension


class CustomExtensionToolbar(ExtensionToolbar):
    def get_title_extension_admin(self, language=None):
        """
        Get the admin urls for the title extensions menu items, depending on whether a TitleExtension instance exists
        for each Title in the current page.
        A single language can be passed to only work on a single title.

        Return a list of tuples of the title extension and the url; the extension is None if no instance exists,
        the url is None is no admin is registered for the extension.
        """
        page = self._get_page()
        urls = []
        if language:
            titles = page.get_title_object(language),
        else:
            titles = page.title_set.all()
        # Titles
        for title in titles:
            try:
                title_extension = self.model.objects.get(extended_object_id=title.pk)
            except self.model.DoesNotExist:
                title_extension = None
            try:
                if title_extension:
                    admin_url = admin_reverse(
                        '%s_%s_change' % (self.model._meta.app_label, self.model._meta.model_name),
                        args=(title_extension.pk,))
                else:
                    admin_url = "%s?extended_object=%s" % (
                        admin_reverse('%s_%s_add' % (self.model._meta.app_label, self.model._meta.model_name)),
                        title.pk)
            except NoReverseMatch:  # pragma: no cover
                admin_url = None
            if admin_url:
                urls.append((title_extension, admin_url, title.title))
        return urls


@toolbar_pool.register
class IconExtensionToolbar(ExtensionToolbar):
    # defineds the model for the current toolbar
    model = PageAttributesExtension

    def populate(self):
        # setup the extension toolbar with permissions and sanity checks
        current_page_menu = self._setup_extension_toolbar()
        # if it's all ok
        if current_page_menu:
            # retrieves the instance of the current extension (if any) and the toolbar item url
            page_extension, url = self.get_page_extension_admin()
            if url:
                # adds a toolbar item
                current_page_menu.add_modal_item(_('Page Attributes'), url=url, disabled=not self.toolbar.edit_mode)


@toolbar_pool.register
class SEOMetaDataExtensionToolbar(CustomExtensionToolbar):
    model = SEOMetaDataExtension

    def populate(self):
        current_page_menu = self._setup_extension_toolbar()

        if current_page_menu and self.toolbar.edit_mode:
            position = 0
            sub_menu = self._get_sub_menu(current_page_menu, 'submenu_label', 'SEO', position)
            urls = self.get_title_extension_admin()

            for title_extension, url, title in urls:
                # adds toolbar items
                sub_menu.add_modal_item('Meta Tags for  %s' % title, url=url, disabled=not self.toolbar.edit_mode)


@toolbar_pool.register
class MetaCanonicalExtensionToolbar(CustomExtensionToolbar):
    # setup the extension toolbar with permissions and sanity checks
    model = MetaCanonicalExtension

    def populate(self):
        # setup the extension toolbar with permissions and sanity checks
        current_page_menu = self._setup_extension_toolbar()
        # if it's all ok
        if current_page_menu and self.toolbar.edit_mode:
            # create a sub menu
            position = 0
            sub_menu = self._get_sub_menu(current_page_menu, 'submenu_label', 'SEO', position)
            # retrieves the instances of the current title extension (if any) and the toolbar item url
            urls = self.get_title_extension_admin()
            # cycle through the title list
            for title_extension, url, title in urls:
                # adds toolbar items
                sub_menu.add_modal_item('Canonical URL for  %s' % title, url=url, disabled=not self.toolbar.edit_mode)
