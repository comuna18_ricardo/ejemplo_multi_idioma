from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from .menu import ServicesSubMenu


class ServicesApp(CMSApp):
    name = _('Services')
    urls = ['services.urls', ]
    app_name = 'services'
    menus = [ServicesSubMenu, ]

apphook_pool.register(ServicesApp)
