# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='image',
            field=filer.fields.image.FilerImageField(to='filer.Image', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='service_image'),
            preserve_default=True,
        ),
    ]
