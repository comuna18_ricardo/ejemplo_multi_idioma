# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import filer.fields.image
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=256, default='')),
                ('slug', models.SlugField(max_length=320, default='')),
                ('content', redactor.fields.RedactorField(verbose_name='Contenido del Servicio')),
                ('language', models.CharField(max_length=16, choices=[('es', 'es'), ('en', 'en')], default='es')),
                ('image', filer.fields.image.FilerImageField(to='filer.Image', null=True, on_delete=django.db.models.deletion.SET_NULL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
