from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView

from .models import Service


class ServiceListView(ListView):
    model = Service

    def render_to_response(self, context, **response_kwargs):

        if self.request.toolbar and self.request.toolbar.edit_mode:
            menu = self.request.toolbar.get_or_create_menu(
                'service-list-menu',
                'Servicios'
            )
            menu.add_sideframe_item(
                u'Listado de Servicios',
                url=reverse('admin:services_service_changelist')
            )
            menu.add_modal_item(
                'Agregar Nuevo Servicios',
                url="%s" % (reverse('admin:services_service_add'), )
            )

        return super(
            ServiceListView,
            self
        ).render_to_response(
            context,
            **response_kwargs
        )


class ServiceDetailView(DetailView):
    model = Service

    def render_to_response(self, context, **response_kwargs):

        if self.request.toolbar and self.request.toolbar.edit_mode:
            menu = self.request.toolbar.get_or_create_menu(
                'service-list-menu',
                'Servicios'
            )
            menu.add_sideframe_item(
                u'Listado de Servicios',
                url=reverse('admin:services_service_changelist')
            )
            menu.add_modal_item(
                'Agregar Nuevo Servicios',
                url="%s" % (reverse('admin:services_service_add'), )
            )

        return super(
            ServiceDetailView,
            self
        ).render_to_response(
            context,
            **response_kwargs
        )
