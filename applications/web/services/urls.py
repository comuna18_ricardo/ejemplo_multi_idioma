from django.conf.urls import patterns, url

from .views import ServiceListView, ServiceDetailView


urlpatterns = patterns(
    '',
    url(r'^$', ServiceListView.as_view(), name="services_list"),
    url(
        r'^(?P<slug>[^/]+)/$',
        ServiceDetailView.as_view(),
        name='services_detail'
    ),
)
