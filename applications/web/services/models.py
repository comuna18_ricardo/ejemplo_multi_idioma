from django.core.urlresolvers import reverse
from django.db import models
from redactor.fields import RedactorField
from filer.fields.image import FilerImageField
from django.conf import settings


class Service(models.Model):
    title = models.CharField(blank=False, max_length=256, default='')
    slug = models.SlugField(blank=False, max_length=320, default='')
    content = RedactorField(
        verbose_name=u'Contenido del Servicio',
        allow_file_upload=False,
        allow_image_upload=True
    )
    image = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='service_image'
    )
    language = models.CharField(max_length=16, choices=getattr(settings, "LANGUAGES", None), default=getattr(settings, "LANGUAGE_CODE", None))

    def __str__(self):
        return self.title

    def absolute_url(self):
        return reverse(
            'services:services_detail',
            kwargs={'slug': self.slug, }
        )
