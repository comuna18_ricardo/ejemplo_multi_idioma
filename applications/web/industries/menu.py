from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.safestring import mark_safe

from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from cms.menu_bases import CMSAttachMenu

from .models import Industry


class IndustrySubMenu(CMSAttachMenu):
    name = _("Industrias SubMenu")

    def get_nodes(self, request):
        nodes = []

        for industry in Industry.objects.filter(translations__language_code=get_language()).order_by('id').all():
            node = NavigationNode(
                mark_safe(industry.name),
                industry.absolute_url(),
                industry.id,
            )
            nodes.append(node)

        return nodes


# menu_pool.register_menu(IndustrySubMenu)
