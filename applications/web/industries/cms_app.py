from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from .menu import IndustrySubMenu


class IndustriesApp(CMSApp):
    name = _("Industrias")
    urls = ["industries.urls"]
    app_name = "industries"
    menus = [IndustrySubMenu, ]


apphook_pool.register(IndustriesApp)
