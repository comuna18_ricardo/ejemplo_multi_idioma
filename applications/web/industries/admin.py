from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import PlaceholderAdminMixin


from .models import Industry


class IndustryAdmin(PlaceholderAdminMixin, TranslatableAdmin):
    list_display = ('name', 'slug', 'language_column')
    fieldsets = (
        (
            _('Translated Fields'), {
                'fields': (
                    'name',
                    'slug',
                    'description',
                )
            }
        ),
        (
            None, {
                'fields': (
                    'image',
                )
            }
        ),
        (
            'Metadata', {
                'classes': ('collapse',),
                'fields': (
                    'meta_title', 
                    'meta_description', 
                    'dcm_subject', 
                    'dcm_keywords',
                    'dcm_source',
                    'publish_date',
                )
            }
        )
    )

    def get_populated_fields(self, request, obj=None):
        return {'slug': ('name',)}


admin.site.register(Industry, IndustryAdmin)
