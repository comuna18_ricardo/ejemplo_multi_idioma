from django.shortcuts import render
from django.views.generic import DetailView, ListView

from django.utils.translation import get_language

from parler.views import TranslatableSlugMixin

from .models import Industry
from products.models import Product


class IndustryListView(ListView):
    model = Industry


class IndustryDetailView(TranslatableSlugMixin, DetailView):
    model = Industry

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        if hasattr(self.request, 'toolbar'):
            self.request.toolbar.set_object(self.object)

        response = super(IndustryDetailView, self).get(*args, **kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(IndustryDetailView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.filter(industries__id=context['industry'].id)
        return context

    def get_queryset(self):
        language = get_language()
        return super(IndustryDetailView, self).get_queryset().filter(
            translations__language_code=language
        )
