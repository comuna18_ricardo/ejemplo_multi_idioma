from django.conf.urls import patterns, url

from .views import IndustryListView, IndustryDetailView


urlpatterns = patterns(
    '',
    url(r'^$', IndustryListView.as_view(), name="industries_list"),
    url(r'^(?P<slug>.*)/$', IndustryDetailView.as_view(), name='industries_detail'),
)