from __future__ import unicode_literals

from django.utils import timezone
from audit_log.models.fields import CreatingUserField

from django.core.urlresolvers import reverse
from django.db import models
from redactor.fields import RedactorField
from filer.fields.image import FilerImageField

from django.utils.translation import ugettext_lazy as _, get_language
from parler.models import TranslatableModel, TranslatedFields
from parler.utils.context import switch_language


class Industry(TranslatableModel):
    class Meta:
        verbose_name = _("Industria")
        verbose_name_plural = _("Industrias")

    translations = TranslatedFields(
        name=models.CharField(blank=False, max_length=256, default=''),
        slug=models.SlugField(blank=False, max_length=320, default=''),
        description=RedactorField(
            verbose_name=u'Descripcion de la industria',
            allow_file_upload=False,
            allow_image_upload=True,
        ),
        # metadatos para el SEO
        meta_title = models.CharField(
            u'Meta Title',
            blank=True,
            max_length=256,
            default='',
            help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)'
        ),
        meta_description = models.TextField(
            u'Meta Description',
            blank=True,
            default='',
            help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)'
        ),
        dcm_subject = models.CharField(
            u'DCM: Subject',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Subject'
        ),
        dcm_keywords = models.CharField(
            u'DCM: Keywords',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Keywords'
        ),
        dcm_source = models.CharField(
            u'DCM: Source',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Source'
        )
    )
    image = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='industrie_image'
    )
    publish_date = models.DateTimeField(
        u'Fecha de Publicacion',
        blank=True,
        default=timezone.now,
        help_text=u'Fecha de Publicacion'
    )
    created_by = CreatingUserField(related_name='created_industrie')

    def __str__(self):
        return self.name

    def absolute_url(self):
        with switch_language(self, get_language()):
            return reverse(
                'industries:industries_detail',
                kwargs={'slug': self.slug, }
            )

    def get_absolute_url(self):
        return reverse(
            'industries:industries_detail',
            kwargs={'slug': self.slug, }
        )