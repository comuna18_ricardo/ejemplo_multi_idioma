# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('image', filer.fields.image.FilerImageField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='filer.Image')),
            ],
            options={
                'verbose_name': 'Industria',
                'verbose_name_plural': 'Industrias',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IndustryTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('language_code', models.CharField(verbose_name='Language', max_length=15, db_index=True)),
                ('name', models.CharField(default='', max_length=256)),
                ('slug', models.SlugField(default='', max_length=320)),
                ('description', models.CharField(default='', max_length=1024)),
                ('master', models.ForeignKey(related_name='translations', null=True, to='industries.Industry', editable=False)),
            ],
            options={
                'db_table': 'industries_industry_translation',
                'managed': True,
                'verbose_name': 'Industria Translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='industrytranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
