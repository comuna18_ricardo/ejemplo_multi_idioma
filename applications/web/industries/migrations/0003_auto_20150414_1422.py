# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('industries', '0002_auto_20150413_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='industry',
            name='image',
            field=filer.fields.image.FilerImageField(related_name='industrie_image', on_delete=django.db.models.deletion.SET_NULL, null=True, to='filer.Image'),
            preserve_default=True,
        ),
    ]
