# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.utils.timezone
import audit_log.models.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('industries', '0003_auto_20150414_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='industry',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, to=settings.AUTH_USER_MODEL, related_name='created_industrie'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='industry',
            name='publish_date',
            field=models.DateTimeField(help_text='Fecha de Publicacion', default=django.utils.timezone.now, verbose_name='Fecha de Publicacion', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='industrytranslation',
            name='dcm_keywords',
            field=models.CharField(verbose_name='DCM: Keywords', help_text='Dublin Core Metadata: Keywords', max_length=512, default='', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='industrytranslation',
            name='dcm_source',
            field=models.CharField(verbose_name='DCM: Source', help_text='Dublin Core Metadata: Source', max_length=512, default='', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='industrytranslation',
            name='dcm_subject',
            field=models.CharField(verbose_name='DCM: Subject', help_text='Dublin Core Metadata: Subject', max_length=512, default='', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='industrytranslation',
            name='meta_description',
            field=models.TextField(help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)', default='', verbose_name='Meta Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='industrytranslation',
            name='meta_title',
            field=models.CharField(verbose_name='Meta Title', help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)', max_length=256, default='', blank=True),
            preserve_default=True,
        ),
    ]
