# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('industries', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='industrytranslation',
            name='description',
            field=redactor.fields.RedactorField(verbose_name='Descripcion de la industria'),
            preserve_default=True,
        ),
    ]
