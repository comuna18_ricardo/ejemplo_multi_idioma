from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from cms.toolbar_base import CMSToolbar
from cms.toolbar_pool import toolbar_pool
from cms.toolbar.items import Break, SubMenu
from cms.cms_toolbar import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK


@toolbar_pool.register
class VideosToolbar(CMSToolbar):

    def populate(self):
        admin_menu = self.toolbar.get_or_create_menu(ADMIN_MENU_IDENTIFIER, _('Apps'))

        position = admin_menu.get_alphabetical_insert_position(
            _('Videos'),
            SubMenu
        )

        if not position:
            position = admin_menu.find_first(Break, identifier=ADMINISTRATION_BREAK) + 1
            admin_menu.add_break('custom-break', position=position)

        menu = admin_menu.get_or_create_menu('videos-menu', _('Videos ...'), position=position)

        # pluginid = 1
        # url = reverse('admin:sliders_slider_change', args=[pluginid])
        # menu.add_modal_item(_('Modificar Slider de esta Pagina'), url=url)

        # menu.add_break()

        url = reverse('admin:videos_video_changelist')
        menu.add_sideframe_item(_('Listado de Videos'), url=url)

        url = reverse('admin:videos_video_add')
        menu.add_modal_item(_('Agregar Nuevo Video'), url=url)
