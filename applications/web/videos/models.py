from django.db import models

from redactor.fields import RedactorField
from django.utils import timezone


class Video(models.Model):

    videofile = models.FileField(upload_to='videos')
    image = models.ImageField(upload_to='previews', default='')
    title = models.CharField(blank=False, max_length=256, default='')
    description = RedactorField(
        verbose_name=u'Titulo de servicio para plugin',
        allow_file_upload=False,
        allow_image_upload=True
    )
    date = models.DateTimeField(
        u'video date',
        blank=False,
        default=timezone.now
    )

    class Meta:
        verbose_name = "Video"
        verbose_name_plural = "Videos"

    def __str__(self):
        return self.title

    def __str__(self):
        pass
