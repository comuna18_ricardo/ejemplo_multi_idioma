from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from .models import Video


class CMSVideosPlugin(CMSPluginBase):
	model = CMSPlugin
	name = u'Plugin de Videos'
	render_template = 'plugins/video_plugin.html'

	def render(self, context, instance, placeholder):
		context['videos'] = Video.objects.order_by("id").reverse()[:4]
		return context

plugin_pool.register_plugin(CMSVideosPlugin)
