# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('videofile', models.FileField(upload_to='videos')),
                ('image', models.ImageField(default='', upload_to='previews')),
                ('title', models.CharField(default='', max_length=256)),
                ('description', redactor.fields.RedactorField(verbose_name='Titulo de servicio para plugin')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='video date')),
            ],
            options={
                'verbose_name': 'Video',
                'verbose_name_plural': 'Videos',
            },
            bases=(models.Model,),
        ),
    ]
