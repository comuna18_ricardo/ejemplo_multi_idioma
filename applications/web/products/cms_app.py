from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from .menu import ProductsSubMenu


class ProductsApp(CMSApp):
    name = _("Productos All")
    urls = ["products.urls"]
    app_name = "productsapp"
    # menus = [ProductsSubMenu, ]


apphook_pool.register(ProductsApp)
