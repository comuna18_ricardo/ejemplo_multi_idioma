from __future__ import unicode_literals

from django.utils import timezone
from audit_log.models.fields import CreatingUserField

from django.core.urlresolvers import reverse
from django.db import models
from redactor.fields import RedactorField
from filer.fields.image import FilerImageField

from django.utils.translation import ugettext_lazy as _, get_language
from parler.models import TranslatableModel, TranslatedFields
from parler.utils.context import switch_language

from cms.models import Page, CMSPlugin
from industries.models import Industry


class FamilyProduct(TranslatableModel):
    class Meta:
        verbose_name = _("Familia de Producto")
        verbose_name_plural = _("Familias de Productos")
        ordering = ['weight']

    translations = TranslatedFields(
        name=models.CharField(blank=False, max_length=256, default=''),
        slug=models.SlugField(blank=False, max_length=320, default=''),
        info=RedactorField(
            verbose_name=u'Información de la familia',
            allow_file_upload=False,
            allow_image_upload=True,
        ),
        description=RedactorField(
            verbose_name=u'Descripcion de la familia',
            allow_file_upload=False,
            allow_image_upload=True,
        ),
        # metadatos para el SEO
        meta_title = models.CharField(
            u'Meta Title',
            blank=True,
            max_length=256,
            default='',
            help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)'
        ),
        meta_description = models.TextField(
            u'Meta Description',
            blank=True,
            default='',
            help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)'
        ),
        dcm_subject = models.CharField(
            u'DCM: Subject',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Subject'
        ),
        dcm_keywords = models.CharField(
            u'DCM: Keywords',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Keywords'
        ),
        dcm_source = models.CharField(
            u'DCM: Source',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Source'
        )
    )
    image = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='family_image'
    )
    page_link = models.ForeignKey(
        Page,
        blank=False,
        null=True,
        on_delete=models.SET_NULL
    )
    icon_animated = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='family_icon_animated'
    )
    icon_static = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='family_icon_static'
    )
    publish_date = models.DateTimeField(
        u'Fecha de Publicacion',
        blank=True,
        default=timezone.now,
        help_text=u'Fecha de Publicacion'
    )
    created_by = CreatingUserField(related_name='created_family')
    weight = models.IntegerField(
        blank=False,
        null=True
    )

    def __str__(self):
        return self.name

    def absolute_url(self):
        with switch_language(self, get_language()):
            return reverse(
                'products:familyproduct_detail',
                kwargs={'slug': self.slug, }
            )


class Product(TranslatableModel):
    class Meta:
        verbose_name = _("Producto")
        verbose_name_plural = _("Productos")

    translations = TranslatedFields(
        name=models.CharField(blank=False, max_length=256, default=''),
        slug=models.SlugField(blank=False, max_length=320, default=''),
        description=RedactorField(
            verbose_name=u'Descripcion del producto',
            allow_file_upload=False,
            allow_image_upload=True,
        ),
        # metadatos para el SEO
        meta_title = models.CharField(
            u'Meta Title',
            blank=True,
            max_length=256,
            default='',
            help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)'
        ),
        meta_description = models.TextField(
            u'Meta Description',
            blank=True,
            default='',
            help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)'
        ),
        dcm_subject = models.CharField(
            u'DCM: Subject',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Subject'
        ),
        dcm_keywords = models.CharField(
            u'DCM: Keywords',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Keywords'
        ),
        dcm_source = models.CharField(
            u'DCM: Source',
            blank=True,
            default='',
            max_length=512,
            help_text='Dublin Core Metadata: Source'
        )
    )
    image = FilerImageField(
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='product_image'
    )
    data = RedactorField(
        verbose_name=u'Data',
        allow_file_upload=False,
        allow_image_upload=True,
    )
    family = models.ForeignKey(
        FamilyProduct,
        blank=False,
        null=True,
    )
    industries = models.ManyToManyField(
        Industry,
        blank=False,
        null=True,
    )
    publish_date = models.DateTimeField(
        u'Fecha de Publicacion',
        blank=True,
        default=timezone.now,
        help_text=u'Fecha de Publicacion'
    )
    created_by = CreatingUserField(related_name='created_product')
    

    def absolute_url(self):
        middle = 'catalogo'
        if(get_language() == 'en'):
            middle = 'catalog'

        with switch_language(self, get_language()):
            return '/' + get_language() + '/' + middle + '/' + self.slug
            #return reverse(
            #    'products:product_detail',
            #    kwargs={'slug': self.slug, }
            #)

    def get_absolute_url(self):
        middle = 'catalogo'
        if(get_language() == 'en'):
            middle = 'catalog'

        with switch_language(self, get_language()):
            return '/' + get_language() + '/' + middle + '/' + self.slug
            #return reverse(
            #    'products:product_detail',
            #    kwargs={'slug': self.slug, }
            #)


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='galleryimage' ,blank=False, null=True)
    image = FilerImageField(
            blank=False,
            null=True,
            on_delete=models.SET_NULL,
            related_name='productimage_image'
        )

class ProductByFamilyPlugin(CMSPlugin):
    family = models.ForeignKey(FamilyProduct)
