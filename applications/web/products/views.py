from django.views.generic import DetailView, ListView

from django.utils.translation import get_language

from parler.views import TranslatableSlugMixin

from .models import Product


class ProductListView(ListView):
    model = Product


class ProductDetailView(TranslatableSlugMixin, DetailView):
    model = Product

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        if hasattr(self.request, 'toolbar'):
            self.request.toolbar.set_object(self.object)

        response = super(ProductDetailView, self).get(*args, **kwargs)
        return response

    def get_queryset(self):
        language = get_language()
        return super(ProductDetailView, self).get_queryset().filter(
            translations__language_code=language,
        )
