from django.conf.urls import patterns, url

from .views import ProductDetailView, ProductListView


urlpatterns = patterns(
    '',
    url(r'^$', ProductListView.as_view(), name="product_list"),
    url(r'^(?P<slug>.*)/$', ProductDetailView.as_view(), name='product_detail'),
)
