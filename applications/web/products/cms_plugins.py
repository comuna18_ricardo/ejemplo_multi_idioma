from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from .models import FamilyProduct, ProductByFamilyPlugin, Product


class CMSFamilyProductPagePlugin(CMSPluginBase):
    model = CMSPlugin
    name = u'Familia de Productos - Pagina'
    render_template = "plugins/familyproducts_page.html"

    def render(self, context, instance, placeholder):
        context['familys'] = FamilyProduct.objects.order_by("weight").reverse()
        return context


class CMSProductsByFamily(CMSPluginBase):
    model = ProductByFamilyPlugin
    name = u'Productos por Familias - Listado de Productos'
    render_template = "plugins/productsbyfamily_list.html"

    def render(self, context, instance, placeholder):
        context.update({
            'products': Product.objects.filter(family__id=instance.family.id),
            'object': instance,
            'placeholder': placeholder
        })
        return context


class CMSFamilyCarouselPlugin(CMSPluginBase):
    model = CMSPlugin
    name = u'Familia de Productos - Carousel'
    render_template = "plugins/familyproducts_carousel.html"

    def render(self, context, instance, placeholder):
        context['familys'] = FamilyProduct.objects.all()
        return context


plugin_pool.register_plugin(CMSFamilyProductPagePlugin)
plugin_pool.register_plugin(CMSProductsByFamily)
plugin_pool.register_plugin(CMSFamilyCarouselPlugin)
