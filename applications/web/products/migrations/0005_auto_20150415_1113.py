# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import audit_log.models.fields
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('products', '0004_auto_20150414_0042'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(related_name='created_product', editable=False, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='dcm_keywords',
            field=models.CharField(help_text='Dublin Core Metadata: Keywords', blank=True, max_length=512, default='', verbose_name='DCM: Keywords'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='dcm_source',
            field=models.CharField(help_text='Dublin Core Metadata: Source', blank=True, max_length=512, default='', verbose_name='DCM: Source'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='dcm_subject',
            field=models.CharField(help_text='Dublin Core Metadata: Subject', blank=True, max_length=512, default='', verbose_name='DCM: Subject'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='meta_description',
            field=models.TextField(help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)', blank=True, verbose_name='Meta Description', default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='meta_title',
            field=models.CharField(help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)', blank=True, max_length=256, default='', verbose_name='Meta Title'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='publish_date',
            field=models.DateTimeField(help_text='Fecha de Publicacion', blank=True, verbose_name='Fecha de Publicacion', default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
