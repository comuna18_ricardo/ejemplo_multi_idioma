# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import audit_log.models.fields
from django.conf import settings
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('products', '0006_auto_20150415_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='familyproduct',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(related_name='created_family', to=settings.AUTH_USER_MODEL, editable=False, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familyproduct',
            name='publish_date',
            field=models.DateTimeField(blank=True, verbose_name='Fecha de Publicacion', help_text='Fecha de Publicacion', default=django.utils.timezone.now),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familyproducttranslation',
            name='dcm_keywords',
            field=models.CharField(blank=True, verbose_name='DCM: Keywords', help_text='Dublin Core Metadata: Keywords', default='', max_length=512),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familyproducttranslation',
            name='dcm_source',
            field=models.CharField(blank=True, verbose_name='DCM: Source', help_text='Dublin Core Metadata: Source', default='', max_length=512),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familyproducttranslation',
            name='dcm_subject',
            field=models.CharField(blank=True, verbose_name='DCM: Subject', help_text='Dublin Core Metadata: Subject', default='', max_length=512),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familyproducttranslation',
            name='meta_description',
            field=models.TextField(blank=True, verbose_name='Meta Description', help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)', default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familyproducttranslation',
            name='meta_title',
            field=models.CharField(blank=True, verbose_name='Meta Title', help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)', default='', max_length=256),
            preserve_default=True,
        ),
    ]
