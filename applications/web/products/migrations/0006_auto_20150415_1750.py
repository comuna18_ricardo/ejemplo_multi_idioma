# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_auto_20150415_1113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='dcm_keywords',
        ),
        migrations.RemoveField(
            model_name='product',
            name='dcm_source',
        ),
        migrations.RemoveField(
            model_name='product',
            name='dcm_subject',
        ),
        migrations.RemoveField(
            model_name='product',
            name='meta_description',
        ),
        migrations.RemoveField(
            model_name='product',
            name='meta_title',
        ),
        migrations.AddField(
            model_name='producttranslation',
            name='dcm_keywords',
            field=models.CharField(help_text='Dublin Core Metadata: Keywords', max_length=512, verbose_name='DCM: Keywords', blank=True, default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='producttranslation',
            name='dcm_source',
            field=models.CharField(help_text='Dublin Core Metadata: Source', max_length=512, verbose_name='DCM: Source', blank=True, default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='producttranslation',
            name='dcm_subject',
            field=models.CharField(help_text='Dublin Core Metadata: Subject', max_length=512, verbose_name='DCM: Subject', blank=True, default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='producttranslation',
            name='meta_description',
            field=models.TextField(help_text='Contenido para meta description (si se deja en blanco se usa el campo Intro)', default='', verbose_name='Meta Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='producttranslation',
            name='meta_title',
            field=models.CharField(help_text='Contenido para meta title (si se deja en blanco se usa el campo Titulo)', max_length=256, verbose_name='Meta Title', blank=True, default=''),
            preserve_default=True,
        ),
    ]
