# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import filer.fields.image
import redactor.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        #('cms', '0004_auto_20150326_1510'),
        ('industries', '0002_auto_20150413_1459'),
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FamilyProduct',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('icon_animated', filer.fields.image.FilerImageField(to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, null=True, related_name='family_icon_animated')),
                ('icon_static', filer.fields.image.FilerImageField(to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, null=True, related_name='family_icon_static')),
                ('image', filer.fields.image.FilerImageField(to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, null=True, related_name='family_image')),
                ('page_link', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, null=True, to='cms.Page')),
            ],
            options={
                'verbose_name_plural': 'Familias de Productos',
                'verbose_name': 'Familia de Producto',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FamilyProductTranslation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('language_code', models.CharField(db_index=True, max_length=15, verbose_name='Language')),
                ('name', models.CharField(max_length=256, default='')),
                ('slug', models.SlugField(max_length=320, default='')),
                ('info', tinymce.models.HTMLField()),
                ('description', redactor.fields.RedactorField(verbose_name='Descripcion de la familia')),
                ('master', models.ForeignKey(editable=False, to='products.FamilyProduct', null=True, related_name='translations')),
            ],
            options={
                'managed': True,
                'db_tablespace': '',
                'default_permissions': (),
                'db_table': 'products_familyproduct_translation',
                'verbose_name': 'Familia de Producto Translation',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('data', tinymce.models.HTMLField()),
                ('family', models.ForeignKey(null=True, to='products.FamilyProduct')),
                ('image', filer.fields.image.FilerImageField(to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, null=True, related_name='product_image')),
                ('industries', models.ManyToManyField(null=True, to='industries.Industry')),
            ],
            options={
                'verbose_name_plural': 'Productos',
                'verbose_name': 'Producto',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductByFamilyPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', parent_link=True)),
                ('family', models.ForeignKey(to='products.FamilyProduct')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('image', filer.fields.image.FilerImageField(to='filer.Image', on_delete=django.db.models.deletion.SET_NULL, null=True, related_name='productimage_image')),
                ('product', models.ForeignKey(to='products.Product', null=True, related_name='galleryimage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductTranslation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('language_code', models.CharField(db_index=True, max_length=15, verbose_name='Language')),
                ('name', models.CharField(max_length=256, default='')),
                ('slug', models.SlugField(max_length=320, default='')),
                ('description', redactor.fields.RedactorField(verbose_name='Descripcion del producto')),
                ('master', models.ForeignKey(editable=False, to='products.Product', null=True, related_name='translations')),
            ],
            options={
                'managed': True,
                'db_tablespace': '',
                'default_permissions': (),
                'db_table': 'products_product_translation',
                'verbose_name': 'Producto Translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='producttranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='familyproducttranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
