# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='familyproducttranslation',
            name='info',
            field=redactor.fields.RedactorField(verbose_name='Información de la familia'),
            preserve_default=True,
        ),
    ]
