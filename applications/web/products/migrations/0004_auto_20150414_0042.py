# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_auto_20150414_0040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='data',
            field=redactor.fields.RedactorField(verbose_name='Data'),
            preserve_default=True,
        ),
    ]
