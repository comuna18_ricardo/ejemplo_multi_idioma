# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_auto_20150416_1907'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='familyproduct',
            options={'ordering': ['publish_date'], 'verbose_name': 'Familia de Producto', 'verbose_name_plural': 'Familias de Productos'},
        ),
    ]
