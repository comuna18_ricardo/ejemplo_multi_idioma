# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_auto_20150422_1807'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='familyproduct',
            options={'verbose_name': 'Familia de Producto', 'verbose_name_plural': 'Familias de Productos'},
        ),
    ]
