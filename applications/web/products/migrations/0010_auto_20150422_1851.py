# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0009_auto_20150422_1839'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='familyproduct',
            options={'verbose_name': 'Familia de Producto', 'ordering': ['publish_date'], 'verbose_name_plural': 'Familias de Productos'},
        ),
    ]
