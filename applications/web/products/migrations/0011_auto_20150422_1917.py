# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0010_auto_20150422_1851'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='familyproduct',
            options={'verbose_name_plural': 'Familias de Productos', 'verbose_name': 'Familia de Producto', 'ordering': ['weight']},
        ),
        migrations.AddField(
            model_name='familyproduct',
            name='weight',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
