from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import PlaceholderAdminMixin

from .models import FamilyProduct, Product, ProductImage


class FamilyProductAdmin(PlaceholderAdminMixin, TranslatableAdmin):
    list_display = ('name', 'slug', 'language_column')
    fieldsets = (
        (
            _('Translated Fields'), {
                'fields': (
                    'name',
                    'slug',
                    'description',
                    'info',
                    'weight',
                )
            }
        ),
        (
            None, {
                'fields': (
                    'image',
                    'icon_animated',
                    'icon_static',
                    'page_link',
                )
            }
        ),
        (
            'Metadata', {
                'classes': ('collapse',),
                'fields': (
                    'meta_title', 
                    'meta_description', 
                    'dcm_subject', 
                    'dcm_keywords',
                    'dcm_source',
                    'publish_date',
                )
            }
        )
    )

    def get_populated_fields(self, request, obj=None):
        return {'slug': ('name',)}


class ProductImageInline(admin.StackedInline):
    model = ProductImage
    extra = 3


class ProductAdmin(PlaceholderAdminMixin, TranslatableAdmin):
    inlines = [ProductImageInline, ]
    list_display = ('name', 'slug', 'language_column')
    fieldsets = (
        (
            _('Translated Fields'), {
                'fields': (
                    'name',
                    'slug',
                    'description',
                    #'data',
                )
            }
        ),
        (
            _('Required Fields'), {
                'fields': (
                    'image',
                    'family',
                    'industries',
                    'data',
                )
            }
        ),
        (
            'Metadata', {
                'classes': ('collapse',),
                'fields': (
                    'meta_title', 
                    'meta_description', 
                    'dcm_subject', 
                    'dcm_keywords',
                    'dcm_source',
                    'publish_date',
                )
            }
        )
    )
    filter_vertical = ('industries',)

    def get_populated_fields(self, request, obj=None):
        return {'slug': ('name',)}


admin.site.register(FamilyProduct, FamilyProductAdmin)
admin.site.register(Product, ProductAdmin)