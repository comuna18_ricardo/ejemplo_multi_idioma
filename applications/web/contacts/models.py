import smtplib

from django.db import models
from django.utils import timezone
from django.template.loader import render_to_string

from cms.models import CMSPlugin


class Contact(models.Model):
    COUNTRIES = (('MX','Mexico'),('EX','Otro'),)
    name = models.CharField(
        u'Nombre Completo',
        blank=True,
        default='',
        help_text=u'Nombre Completo',
        max_length=256,
    )
    company = models.CharField(
        u'Empresa u Organizacion',
        blank=True,
        default='',
        help_text=u'Nombre de su Empresa',
        max_length=128,
    )
    email = models.EmailField(
        u'Email',
        blank=True,
        default='',
        error_messages={'invalid': 'Introduzca un email valido'},
        help_text=u'Su Email',
        max_length=128,
    )
    country = models.CharField(
        u'Pais',
        max_length=64,
        choices=COUNTRIES,
        blank=True
    )
    telephone = models.CharField(
        u'Telefono',
        blank=True,
        default='',
        help_text=u'Introduzca su Numero Telefonico',
        max_length=32,
    )
    comments = models.TextField(
        u'Mensaje',
        blank=True,
        default='',
        help_text=u'Aqui va su mensaje.',
    )
    send_to = models.CharField(
        u'Enviado desde:',
        blank=True,
        default='',
        help_text='Link donde se mando el correo',
        max_length=256,
    )
    contact_date = models.DateTimeField(
        u'contact date',
        blank=True,
        default=timezone.now,
        help_text=u'Cuando se realizo este contacto.',
    )

    def send_notification_email(self):
        email_subject = render_to_string('contacts/notification-subject.txt', {
            'contact': self,
        })

        email_body = render_to_string('contacts/notification-body.txt', {
            'contact': self,
        })
        #print email_subject;
        #print email_body;
        try:
            #fromaddr = 'sumicorpdemexico@gmail.com'
            fromaddr = 'info@sumicorp.com.mx'
            toaddrs  = ['edson@evolutiva.mx', 'ev.sumicorp@gmail.com', 'jflores@sumicorp.com.mx', 'btorres@sumicorp.com.mx', 'eflores@sumicorp.com.mx']
            subject = "Subject: " + email_subject
            #msg = "\r\n".join([ "From: sumicorpdemexico@gmail.com", "To: " + ", ".join(toaddrs), subject, "", email_body ])
            msg = "\r\n".join([ "From: info@sumicorp.com.mx", "To: " + ", ".join(toaddrs), subject, "", email_body ])
            #username = 'sumicorpdemexico@gmail.com'
            username = 'info@sumicorp.com.mx'
            #password = 'SK6b9PeXb743V6c'
            password = 'sumicorp'
            #server = smtplib.SMTP('smtp.gmail.com:587')
            server = smtplib.SMTP('smtp.sumicorp.com.mx:25')
            server.ehlo()
            #server.starttls()
            server.login(username,password)
            server.sendmail(fromaddr, toaddrs, msg)
            server.quit()

        except Exception:
            # If NOT in DEBUG (development) mode, we silently ignore any
            # exceptions to avoid interrupting capture of the submitter's
            # details. If in DEBUG mode, then raise the error so we can
            # troubleshoot.
            if (settings.DEBUG):
                raise

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                self.send_notification_email()
            except:
                pass
        super(Contact, self).save(*args, **kwargs)

    def __str__(self):
        return '%s (%s)' % (self.name, str(self.contact_date), )


class ContactPluginModel(CMSPlugin):
    title = models.CharField(
        u'title',
        blank=True,
        help_text=u'Opcional. Titulo del Widget',
        max_length=64,
    )
