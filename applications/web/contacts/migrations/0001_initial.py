# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        #('cms', '0004_auto_20150326_1510'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256, default='', blank=True, help_text='Nombre Completo', verbose_name='Nombre Completo')),
                ('company', models.CharField(max_length=128, default='', blank=True, help_text='Nombre de su Empresa', verbose_name='Empresa u Organizacion')),
                ('email', models.EmailField(max_length=128, default='', blank=True, help_text='Su Email', error_messages={'invalid': 'Introduzca un email valido'}, verbose_name='Email')),
                ('country', models.CharField(max_length=64, blank=True, verbose_name='Pais', choices=[('MX', 'Mexico'), ('EX', 'Otro')])),
                ('telephone', models.CharField(max_length=32, default='', blank=True, help_text='Introduzca su Numero Telefonico', verbose_name='Telefono')),
                ('comments', models.TextField(default='', blank=True, help_text='Aqui va su mensaje.', verbose_name='Mensaje')),
                ('contact_date', models.DateTimeField(default=django.utils.timezone.now, blank=True, help_text='Cuando se realizo este contacto.', verbose_name='contact date')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        #migrations.CreateModel(
        #    name='ContactPluginModel',
        #    fields=[
        #        ('cmsplugin_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, to='cms.CMSPlugin', serialize=False)),
        #        ('title', models.CharField(max_length=64, blank=True, help_text='Opcional. Titulo del Widget', verbose_name='title')),
        #    ],
        #    options={
        #        'abstract': False,
        #    },
        #    bases=('cms.cmsplugin',),
        #),
    ]
