# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='send_to',
            field=models.CharField(blank=True, help_text='Link donde se mando el correo', max_length=256, default='', verbose_name='Enviado desde:'),
            preserve_default=True,
        ),
    ]
