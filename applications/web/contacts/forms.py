from django import forms
from django.forms import ModelForm

from .models import Contact


class ContactBaseForm(ModelForm):
    class Meta:
        abstract = True

    required_fields = []

    def __init__(self, *args, **kwargs):
        super(ContactBaseForm, self).__init__(*args, **kwargs)

        for field in self.required_fields:
            self.fields[field].required = True


class ContactForm(ContactBaseForm):
    class Meta:
        model = Contact
        fields = [
            'name', 'company', 'email', 'country',
            'telephone', 'comments',
             'acepto_aviso_de_privacidad', 'send_to',
        ]

    acepto_aviso_de_privacidad = forms.BooleanField()
    send_to = forms.CharField(widget = forms.HiddenInput())

    required_fields = ['name', 'email', 'company', 'telephone', 'comments', 'acepto_aviso_de_privacidad', 'country', 'send_to',]

    def clean(self):
        cleaned_data = super(ContactForm, self).clean()
        privacyterms = cleaned_data.get('acepto_aviso_de_privacidad')

        if privacyterms is not True:
            raise forms.ValidationError(u'Debe aceptar los terminos de privacidad')

        return cleaned_data


class ContactAjaxForm(ContactBaseForm):
    class Meta:
        model = Contact
        fields = ['name', 'company', 'telephone', 'email', 'comments', 'send_to' ]

    send_to = forms.CharField(widget = forms.HiddenInput())

    required_fields = ['name', 'company', 'telephone', 'email', 'comments', 'send_to' ]


class ContactAjaxFormModal(ContactBaseForm):
    class Meta:
        model = Contact
        fields = ['name', 'company', 'country','telephone', 'email', 'comments', 'send_to',]

    send_to = forms.CharField(widget = forms.HiddenInput())

    required_fields = ['name', 'company', 'country','telephone', 'email', 'comments', 'send_to',]
