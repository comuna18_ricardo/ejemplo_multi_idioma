import json

from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView
from django.shortcuts import get_object_or_404

from cms.models.pagemodel import Page

from .forms import ContactForm, ContactAjaxForm


class ContactFormView(FormView):
    form_class = ContactForm
    template_name = 'contacts/contact_form.html'

    def get_success_url(self):
        page = get_object_or_404(
            Page,
            reverse_id="contact_form_submission",
            publisher_is_draft=False
        )
        return page.get_absolute_url()

    def form_valid(self, form):
        self.object = form.save()
        return super(ContactFormView, self).form_valid(form)


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors)  # , status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return self.render_to_json_response(data)
        else:
            return response


class ContactFormAjaxView(AjaxableResponseMixin, FormView):
    form_class = ContactAjaxForm
    http_method_names = [u'post']
    template_name = 'contacts/contact_widget.html'

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        self.object = form.save(commit=True)
        return super(ContactFormAjaxView, self).form_valid(form)
