# Directorio de Aplicaciones

En este directorio se guardan todas las aplicaciones, eso incluye:

  - Sitios Web -> web
  - Sistemas -> system
  - APIS -> api
  - CMS's -> cms
  - Etc.

Cada aplicación en una carpeta distinta. Este directorio incluye 2 carpetas, una con 
configuraciones y otra para los datos incluyendo las bases de datos.

  - config
  - data
