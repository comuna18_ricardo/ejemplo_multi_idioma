#!/usr/bin/env bash

cp templates/web/site/js/site.js applications/web/sumicorpcommx/static/site/js/site.js
cp templates/web/site/css/site.min.css applications/web/sumicorpcommx/static/site/css/site.min.css
