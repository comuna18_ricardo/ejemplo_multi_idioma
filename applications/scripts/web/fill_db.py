#!/usr/bin/env python
import os, sys, datetime

now = datetime.datetime.now()
django_dir =  os.path.abspath(os.path.join(os.path.join(os.path.dirname(__file__),"../.."), 'web/sumicorpcommx'))
backup_dir =  os.path.abspath(os.path.join(os.path.join(os.path.dirname(__file__),"../.."), 'data/db/' + str(now.year)))

if django_dir not in sys.path:
    sys.path.insert(0, django_dir)

import settings

default_db = settings.DATABASES['default']
host_info = settings.HOST_INFO

if not os.path.exists(backup_dir):
    pass #set exception / no backup dir
#datenow = "%d-%s-%s" % (now.year, str(now.month).zfill(2), str(now.day).zfill(2))
#timenow = "%s%s" % (str(notimplementedw.hour).zfill(2), str(now.minute).zfill(2))
#format: osuser.host.dbname.date.hour.sql
#filename = "%s.%s.%s.%s.%s.sql" % (host_info['USER'], host_info['HOST'], default_db['NAME'], datenow, timenow)
command = "ls -Art %s | tail -1" % (backup_dir)
command_rd = os.popen(command).readlines()

try:
    filename = command_rd[0]
except IndexError:
    raise NameError('Error al abrir backup')

command = "export PGPASSWORD=%s\npsql -h %s -U %s %s < %s/%s" % (default_db['PASSWORD'], default_db['HOST'], default_db['USER'], default_db['NAME'], backup_dir, filename)
os.system(command)
