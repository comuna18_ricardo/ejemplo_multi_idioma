# Directorio de Scripts

En este directorio se guardan los scripts que se ejecutaran para
diversas tareas, como:

    - Respaldo de la db
    - Actualizacion de la db
    - Ejecucion del docker
    - Ejecucion de un contenedor, etc...

Cada script debe guardarse en una carpeta, correspondiente a su
tipo de ejecucion.

Ejemplo:

    - db/respaldo_db.sh
    - db/actualizar_db.sh
    - app/docker.sh
