# Directorio de Configuraciones

Esta carpeta contiene las configuraciones y/o software requerido para el correcto 
funcionamiento de todas las  aplicaciones, cada una de ellas tendrá una carpeta
independiente. 

Ejemplos de archivos de configuración:

  - requirements.txt
  - setting
  - plugins
  - *.zip, *.tar.gz, *.*
