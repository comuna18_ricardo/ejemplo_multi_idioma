# Directorio de Datos

La nomenclatura para los dumps de las bases de datos es:

```sh
usuario.host.fecha-ISO.hora.sql
```

ejemplo:

```sh
rolvera.localhost.2015-12-01.2045.sql
```