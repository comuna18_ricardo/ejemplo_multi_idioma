/**
* Created with sumicorp.mx.
* User: sachiel
* Date: 2014-11-25
* Time: 07:48 AM
*/

//Modify host and menu_items
var host = 'evolutiva.mx';
var web_server = 'http://' + host + '/';

var menu_items = [
    {'link': 'http://evolutiva.mx/', 'name':'Evolutiva'},
    {'link': 'http://localhost/sumicorp_v3/templates/web/', 'name':'Template'},
    {'link': 'http://localhost:8080', 'name':'Desarrollo'},
];

    $(document).ready(function(){
    var elem = $('#menu-home-item');
    var menu_items_html = '';

    $('#menu-home-item > a').attr('href', web_server);

    for(var i = 0; i < menu_items.length; i++){
        menu_items_html += '<li><a href="' + menu_items[i].link + '">'+ menu_items[i].name +'</a></li>'
    }

    elem.after(menu_items_html);
})
